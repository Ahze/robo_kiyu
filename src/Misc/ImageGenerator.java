package Misc;

import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import rpg.Player;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageGenerator {

    static int count = 0;

    private Player player;
    private int id;
    private String nom;


    public ImageGenerator(Player player){
        this.player = player;
        this.id = count++;
        try {
            this.generateImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generateImage() throws IOException {
        this.nom = player.getName()+"#"+player.getUser().getDiscriminator();
        BufferedImage im = new BufferedImage(400, 180, BufferedImage.TYPE_INT_RGB);
        Graphics g = im.getGraphics();
        g.setColor(new Color(54, 57, 62));
        g.fillRect(0, 0, 400, 180);

        Image avatar = getImageFromUrl(this.player.getUser().getAvatarUrl());
        g.drawImage(avatar, 10, 10, null);

        g.setColor(new Color(79, 79, 79));
        g.fillRoundRect(10, 150, 380, 20, 4,4);
        g.setColor(new Color(196, 18, 0));
        double percent = (double)player.getXp() / (double)player.xpToNextLevel();
        g.fillRoundRect(11, 151, (int)(380d*percent), 18, 4,4);

        g.setColor(Color.white);
        g.setFont(new Font("Courier", Font.BOLD, 16));
        g.drawString(""+this.player.getXp(), 14, 151+16);
        g.drawString(""+this.player.xpToNextLevel(), 350, 151+16);

        g.setColor(new Color(233, 35, 255));
        g.drawString("leuvail: "+this.player.getLevel(), 10+128+10, 50);
        g.setColor(new Color(216, 85, 89));
        g.drawString("hp maks: "+this.player.getHp(), 10+128+10, 70);
        g.setColor(Color.green);
        g.drawString("atak: "+this.player.getAtk(), 10+128+10, 90);
        g.setColor(new Color(34, 96, 255));
        g.drawString("daifenss: "+this.player.getDef(), 10+128+10, 110);
        g.setColor(new Color(167, 194, 13));
        g.drawString("po1 de konpaitans: "+this.player.getSkillPoints(), 10+128+10, 130);

        g.setColor(Color.white);
        g.setFont(new Font("Courier", Font.PLAIN, 20));
        g.drawString(nom, 10+128+10, 30);



        g.dispose();

        File outputfile = new File(nom+".jpg");
        ImageIO.write(im, "jpg", outputfile);
    }

    public void sendImage(MessageChannel channel){
        File image = new File(this.nom+".jpg");
        Message message = new MessageBuilder().append(" ").build();
        channel.sendFile(image, message).queue();
        image.delete();
    }

    private static Image getImageFromUrl(String urlStr) throws IOException {
        final URL url = new URL(urlStr);
        final HttpURLConnection connection = (HttpURLConnection) url
                .openConnection();
        connection.setRequestProperty(
                "User-Agent",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");
        return ImageIO.read(connection.getInputStream());
    }

}
