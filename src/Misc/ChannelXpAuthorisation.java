package Misc;

import Database.DBConnection;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ChannelXpAuthorisation {

    private MessageChannel channel;
    private boolean authorized;

    public ChannelXpAuthorisation(MessageChannel channel){
        this.channel = channel;
        this.authorized = true;
        try{
            Connection c = DBConnection.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM ChannelXp WHERE CHANNEL_ID = ?");
            ps.setString(1, channel.getId());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.authorized = rs.getBoolean("AUTHORIZED");
            }
            else{
                this.saveNew();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void save(){
        Connection c = DBConnection.getConnection();
        try{
            PreparedStatement statement = c.prepareStatement("UPDATE ChannelXp set AUTHORIZED = ? WHERE CHANNEL_ID = ? ");
            statement.setBoolean(1, this.authorized);
            statement.setString(2, this.channel.getId());
            statement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    private void saveNew(){
        Connection c = DBConnection.getConnection();
        try{
            PreparedStatement statement = c.prepareStatement("INSERT INTO ChannelXp(Channel_ID) VALUES(?)");
            statement.setString(1, this.channel.getId());
            statement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean a){
        this.authorized = a;
        this.save();
        this.channel.sendMessage("L'xp sure se chanel hai "+(this.authorized ? "actiV" : "D zactiV")).queue();
    }

    public static void createTable(){
        Connection c = DBConnection.getConnection();
        try {
            c.createStatement().executeUpdate(
                    "CREATE TABLE IF NOT EXISTS ChannelXp ("+
                            " CHANNEL_ID VARCHAR(50) PRIMARY KEY,"+
                            " AUTHORIZED boolean DEFAULT TRUE"+
                            ")");
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

}
