package XpManager;

import Bot.MessageManager;
import Misc.ChannelXpAuthorisation;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import rpg.Player;

public class MessageXpManager implements MessageManager {

    @Override
    public void manage(MessageChannel channel, User user, String message) {
        ChannelXpAuthorisation channelXp = new ChannelXpAuthorisation(channel);
        if(channelXp.isAuthorized()){
            new Player(user).gainXp(1, channel);
        }

        if(message.equals("k!xp-on")){
            channelXp.setAuthorized(true);
        }
        else if(message.equals("k!xp-off")){
            channelXp.setAuthorized(false);
        }
    }
}
