package Bot;

import Misc.ImageGenerator;
import XpManager.MessageXpManager;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import rpg.Player;
import rpg.RpgManager;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.function.Consumer;

public class Bot extends ListenerAdapter {

    private JDA jda;
    private final String prefix = "k!";

    private MessageManager rpgManager, messageXpManager, afkManager, voteManager;

    public Bot(String token) throws Exception{
        this.jda = new JDABuilder(AccountType.BOT).setToken(token).buildBlocking();
        this.jda.addEventListener(this);
        this.jda.getPresence().setGame(Game.of(Game.GameType.DEFAULT, "KROKET"));

        this.rpgManager = new RpgManager();
        this.messageXpManager = new MessageXpManager();
    }


    @Override
    public void onMessageReceived(MessageReceivedEvent message){
        if(message.getAuthor().isBot())
            return;

        //gestion de l'xp par rapport aux messages
        this.messageXpManager.manage(message.getChannel(), message.getAuthor(), message.getMessage().getContentRaw());


        Player author = new Player(message.getAuthor());

        if(!message.getMessage().getContentRaw().startsWith(prefix))
            return;

        String  content = message.getMessage().getContentRaw().substring(2);

        if(content.equals("help")){
            sendMessageToUser(message.getAuthor(), getHelp());
        }
        else if(content.equals("rank")){
            new ImageGenerator(author).sendImage(message.getChannel());
            //message.getChannel().sendMessage(author.toEmbed().build()).queue();
        }
        else if(content.startsWith("rpg-")){
            if(content.length() > 5){
                rpgManager.manage(message.getChannel(), message.getAuthor(), content.substring(4));
            }
        }
        else if(content.startsWith("afk")){
            String raison = content.substring(3);//faut verifier la taille avant
        }
        else if(content.startsWith("vote")){

        }

        System.out.println(this.toString());


    }

    public static void sendMessageToUser(User user, String msg){
        user.openPrivateChannel().queue(new Consumer<PrivateChannel>() {
            @Override
            public void accept(PrivateChannel privateChannel) {
                privateChannel.sendMessage(msg);
            }
        });
    }



    public String getHelp(){
        String res;
        try{
            File file = new File("help.txt");
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            res = new String(data, "UTF-8");
        }
        catch (Exception e){
            res = e.getMessage();
        }
        return res;
    }

    public String toString(){
        String res = "----------------------------------------------\n";
        res += rpgManager.toString();

        return res;
    }


}
