package Bot;

import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public interface MessageManager {

    void manage(MessageChannel channel, User user, String message);

}
