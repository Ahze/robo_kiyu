package rpg;

import Bot.MessageManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.HashMap;

public class RpgManager implements MessageManager {

    /**
     * Attributs de la classe RpgManager
     * fights reprensente tous les combats en cours, chaque combat est accessible par un channel
     */
    private HashMap<MessageChannel, Fight> fights;


    /**
     * Constructeur de la classe RpgManager
     */
    public RpgManager(){
        this.fights = new HashMap<>();
    }

    /**
     * Fonction de traitement d'un message
     * @param channel le channel du message
     * @param user l'autheur du message
     * @param message le message en lui même
     */
    @Override
    public void manage(MessageChannel channel, User user, String message) {
        Player p = new Player(user);
        try{
            switch (message){
                case "create":
                    requireFight(false, channel, "Il i a deja 1 komba issi !");
                    createFight(channel, user);
                    break;
                case "join":
                    requirePlayerInFight(false, channel, user, user.getName()+" é deja dan le comba");
                    joinFight(channel, p);
                    break;
                case "start":
                    requirePlayerInFight(true, channel, user, user.getName()+" nez pa dans le comba");
                    startFight(channel);
                    break;
                case "+hp":
                    p.definitiveUpgrade("hp", channel);
                    break;
                case "+atk":
                    p.definitiveUpgrade("atk", channel);
                    break;
                case "+def":
                    p.definitiveUpgrade("def", channel);
                    break;
                case "help":
                    this.help(channel);
                    break;
            }
        }
        catch (Exception e){
            channel.sendMessage(e.getMessage()).queue();
        }

    }


    /*
    ################# Fonctionnalités principales ########################
     */
    private void createFight(MessageChannel channel, User user){
        this.fights.put(channel, new Fight(channel, new Player(user)));
    }

    private void joinFight(MessageChannel channel, Player player){
        this.fights.get(channel).addPlayer(player);
    }

    private void startFight(MessageChannel channel) {
        this.fights.get(channel).start();
    }



    /*
    #################### Methodes faisant office de précondition
     */

    private void requireFight(boolean isRequired, MessageChannel channel, String errorMessage) throws Exception{
        if(this.fights.containsKey(channel) != isRequired)
            throw new Exception(errorMessage);
    }

    private Player requirePlayerInFight(boolean isRequired, MessageChannel channel, User user, String errorMessage) throws Exception{
        requireFight(true, channel, "Il ni a pa de komba !!");
        if( this.fights.get(channel).hasUser(user) != isRequired){
            throw new Exception(errorMessage);
        }
        if(isRequired){
            return this.fights.get(channel).getPlayer(user);
        }
        else{
            return new Player(user);
        }
    }


    public void help(MessageChannel channel){
        EmbedBuilder eb = new EmbedBuilder();

        eb.setTitle("Ède poure le Rpg");
        eb.addField("k!rpg-create", "créé 1 komba dan le chanel aktuel", false);
        eb.addField("k!rpg-join", "rejoin le komba du chanel aktuel", false);
        eb.addField("k!rpg-start", "daimarr le komba du chanel aktuel", false);
        eb.addField("k!rpg-+hp k!rpg-+atk k!rpg-+def", "ogmente 1 statistik et consom 1 poin de compaitens", false);

        channel.sendMessage(eb.build()).queue();
    }

    public String toString(){
        String res = "Etat du gestionnaire de combat :\n";
        if(this.fights.isEmpty()){
            res += "Aucun combat en cours";
        }
        else{
            for(Fight f : this.fights.values()){
                res += f.toString();
            }
        }
        return res;
    }

}
