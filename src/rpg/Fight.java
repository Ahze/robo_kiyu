package rpg;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Fight {

    /**
     * Enumeration represantant le statut d'un combat
     * WaitingForPlayers : le combat n'a pas encore commencé, on attend que les joueurs rejoignent
     * InFight : en combat contre un monstre
     * BetweenFights : gain d'xp des joueurs entre deux monstres
     */
    enum State{
        WaitingForPlayers("En atante 2 joueure", new String[]{
                "k!rpg-join poure rejo1dr le komba",
                "k!rpg-start poure lanser le komna"
        }),
        InFight("En komba", new String[]{
                "k!atk poure ataker",
                "k!def poure se daifandr",
                "k!maji poure fer de la maji"
        }),
        BetweenFights("Rézolussion du komba", new String[]{

        });

        private String desc;
        private List<String> commandes;

        State(String desc, String[] commandes){
            this.desc = desc;
            this.commandes = new ArrayList<>();
            for(String c : commandes){
                this.commandes.add(c);
            }
        }

        @Override
        public String toString(){
            return this.desc;
        }
    }

    /**
     * Attributs de la classes Fight
     * channel : Le channel ou se déroule le combat
     * players : la liste des joueurs participant au combat
     * state : l'état actuel du combat
     */
    private MessageChannel channel;
    private List<Player> players;
    private Monster monster;
    private State state;


    /**
     * Constructeur de la classe Fight
     * contruit les attributs et envoie un message pour prévenir de l'attente des joueurs
     * @param channel le channel dans lequel se déroule le combat
     * @param player le joueur ayant créé le combat
     */
    public Fight(MessageChannel channel, Player player){
        this.channel = channel;
        this.players = new ArrayList<>();
        this.players.add(player);
        this.state = State.WaitingForPlayers;

        System.out.println("Creation d'un nouveau combat dans le channel "+channel.getName()+" par l'utilisateur "+player.getUser().getName()+"\n");

        EmbedBuilder eb = this.toEmbed();
        eb.setTitle("Kreassion d1 comba sure ce chanel");
        this.sendMessage(eb);
    }


    /**
     * Méthode pour ajouter un joueur au combat
     * @param player
     */
    public void addPlayer(Player player) {
        this.players.add(player);
        EmbedBuilder eb = this.toEmbed();
        eb.setTitle(player.getUser().getName()+" rejo1 le komba !");
        this.sendMessage(eb);
    }

    /**
     * Méthode indiquant si un utilisateur Discord fait parti du combat
     * @param user l'utilisateur
     * @return un booleen
     */
    public boolean hasUser(User user){
        return this.getPlayer(user) != null;
    }

    /**
     * Retourne le joueur (faisant parti du combat) correspondant à l'utilisateur passé en parametre
     * @param user l'utilisateur
     * @return Player
     */
    public Player getPlayer(User user){
        for(Player player : this.players){
            if(player.getUser() == user) {
                return player;
            }
        }
        return null;
    }


    /**
     * Méthode de démarrage du combat
     */
    public void start(){
        this.state = State.InFight;
        EmbedBuilder eb = this.toEmbed();
        eb.setTitle("Daibu du komba !");
        this.sendMessage(eb);

        this.newTurn();
    }


    public void newTurn(){
        this.monster = new Monster(this.players);
        EmbedBuilder ebMonster = this.monster.toEmbed();
        ebMonster.setTitle("Vou zaler afronter se monstr : "+this.monster.getName());
        this.sendMessage(ebMonster);
    }




    public void sendMessage(String msg){
        this.channel.sendMessage(msg).queue();
    }

    public void sendMessage(EmbedBuilder eb){
        this.channel.sendMessage(eb.build()).queue();
    }

    public EmbedBuilder toEmbed(){
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Komba sure le chanail "+this.channel.getName());
        String participants = "";
        for(Player p : this.players)
            participants += p.getUser().getName()+"\n";
        eb.addField("participans", participants, false);
        eb.addField("Éta du konba : ", this.state.toString(), false);

        String commandes = "";
        for (String commande : this.state.commandes) {
            commandes += commande+"\n";
        }

        eb.addField("Comendes possibl", commandes, false);

        eb.setColor(Color.red);
        return eb;
    }

    @Override
    public String toString() {
        String res = "Combat sur le channel "+this.channel.getName()+" :\n";
        for(Player player : this.players){
            res += "\t - "+player.getUser().getName()+"\n";
        }
        return res;
    }
}
