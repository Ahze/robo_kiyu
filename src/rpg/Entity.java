package rpg;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageChannel;

public abstract class Entity {

    protected int atk;
    protected int def;
    protected int hpMax;
    protected int hp;
    protected int level;
    protected int xp;

    public Entity(int atk, int def, int hp, int level, int xp) {
        this.atk = atk;
        this.def = def;
        this.hpMax = this.hp;
        this.hp = hp;
        this.level = level;
        this.xp = xp;
    }

    public Entity(){
        this(1, 1, 1, 1, 0);
    }

    public void gainXp(int xp, MessageChannel channel){
        for(int i = 0; i < xp; i++){
            this.gainOneXp();
        }
    }

    protected boolean gainOneXp(){
        this.xp ++;
        if(this.xp >= this.xpToNextLevel()){
            this.xp = 0;
            this.level++;
            return true;
        }
        return false;
    }

    public int xpToNextLevel(){
        return this.level *15;
    }

    public EmbedBuilder toEmbed(){
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(this.getName());
        eb.addField("leuvèle :", ""+this.level, false);
        eb.addField("hache pet :", ""+this.hp, false);
        eb.addField("atake :", ""+this.atk, false);
        eb.addField("daifense :", ""+this.def, false);
        return eb;
    }




    public void upgrade(String stat){
        switch (stat){
            case "hp":
                this.hp++;
                break;
            case "atk":
                this.atk++;
                break;
            case "def":
                this.def++;
                break;
            default:
                System.out.println("Erreur, augmentation de la statistique : "+stat);
        }
    }

    public int getLevel(){
        return this.level;
    }

    public int getXp(){
        return this.xp;
    }

    public int getHp(){
        return this.hp;
    }

    public int getAtk(){
        return this.atk;
    }

    public int getDef(){
        return this.def;
    }

    public abstract String getName();

}
