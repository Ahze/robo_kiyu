package rpg;

import Database.DBConnection;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.awt.*;
import java.sql.*;
import java.util.ArrayList;

public class Player extends Entity{


	private User user;
	private int skillPoints;

	public Player(User user){
		super();
		this.user = user;
		this.skillPoints = 0;
		try{
			Connection c = DBConnection.getConnection();
			PreparedStatement ps = c.prepareStatement("SELECT * FROM Player WHERE USER_ID = ?");
			ps.setString(1, user.getId());
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				this.atk = rs.getInt("atk");
				this.def = rs.getInt("def");
				this.hp = rs.getInt("hp");
				this.level = rs.getInt("player_level");
				this.xp = rs.getInt("xp");
				this.skillPoints = rs.getInt("skill_points");
			}
			else{
				this.saveNew();
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
	}




	private void saveNew(){
		Connection c = DBConnection.getConnection();
		try{
			PreparedStatement statement = c.prepareStatement("INSERT INTO Player(user_id) VALUES(?)");
			statement.setString(1, this.user.getId());
			statement.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	public void save(){
		Connection c = DBConnection.getConnection();
		try{
			PreparedStatement statement = c.prepareStatement("UPDATE Player set player_level = ?, skill_points = ?, xp = ?, atk = ?, def = ?, hp = ? WHERE user_id = ? ");
			statement.setInt(1, this.level);
			statement.setInt(2, this.skillPoints);
			statement.setInt(3, this.xp);
			statement.setInt(4, this.atk);
			statement.setInt(5, this.def);
			statement.setInt(6, this.hp);
			statement.setString(7, this.user.getId());
			statement.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	public static void createTable(){
		Connection c = DBConnection.getConnection();
		try {
			c.createStatement().executeUpdate(
					"CREATE TABLE IF NOT EXISTS Player ("+
							" USER_ID VARCHAR(50) PRIMARY KEY,"+
							" player_level INT(10) DEFAULT 1,"+
							" skill_points INT(10) DEFAULT 0,"+
							" xp INT(10) DEFAULT 0,"+
							" ATK INT(10) DEFAULT 1,"+
							" DEF INT(10) DEFAULT 1,"+
							" HP INT(10) DEFAULT 2" +
							")");
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}


	public static void dropTable(){
		Connection c = DBConnection.getConnection();
		try {
			c.createStatement().executeUpdate(
					"DROP TABLE IF EXISTS `Player`");
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void gainXp(int xp, MessageChannel channel){
		for(int i = 0; i < xp; i++){
			if(this.gainOneXp()){
				this.skillPoints++;
				channel.sendMessage("bras veau "+this.user.getAsMention()+" ta parelé asé pour aitr o nivo "+this.level+" tu é sure la baun voua poure neu jamé arété daprandr!!").queue();
			}
		}
		this.save();
	}


	public void definitiveUpgrade(String stat, MessageChannel channel){
		if(this.skillPoints > 0){
			this.upgrade(stat);
			this.skillPoints--;
			EmbedBuilder eb = this.toEmbed();
			eb.setTitle(this.getName()+" a ogmanT son "+stat);
			channel.sendMessage(eb.build()).queue();
		}
		else{
			channel.sendMessage("Tu na pa aC 2 po1 2 kompaitans").queue();
		}
		this.save();
	}



	public User getUser(){
		return this.user;
	}

	public String getName(){
		return this.user.getName();
	}

	public int getSkillPoints() {
		return skillPoints;
	}

	@Override
	public EmbedBuilder toEmbed() {
		EmbedBuilder eb = super.toEmbed();
		eb.addField("iks pet avan proch1 nivo", ""+this.xpToNextLevel(), false);
		eb.addField("po1 de kompétans", ""+this.skillPoints, false);
		eb.setColor(Color.red);
		return eb;
	}
}

