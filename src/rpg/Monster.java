package rpg;

import java.util.List;

public class Monster extends Entity {

    private static String[] names = new String[]{

    };

    private String name;

    public Monster(List<Player> players){
        super();
        int xp = 0;
        for(Player p : players)
            xp += p.getXp();
        this.gainXp(xp, null);

        for(int i = 0; i < this.level; i++){
            double statChooser = Math.random();
            if(statChooser < 0.5d)
                this.upgrade("hp");
            else if(statChooser < 0.75d)
                this.upgrade("atk");
            else
                this.upgrade("def");
        }

        this.name = names[(int)(Math.random()*names.length)];

    }

    public String getName(){
        return this.name;
    }

}
