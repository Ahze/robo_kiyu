package Database;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {

    private static Connection connection;

    public static Connection getConnection(){
        if(connection == null)
            connection = createConnection();
        return connection;
    }

    private static Connection createConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/roboKiyu", "root", "root");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


}
